import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'
import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'


baseURL = 'http://localhost:3000/';
//baseURL = 'http://www.trendsetters.com/';



Template.registerHelper( 'Num2String', ( Num ) => {
  return Num.toLocaleString();
});
