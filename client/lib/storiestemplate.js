import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';



Template.storiestemplate.helpers({
	modalStories(){
        return Session.get('modalstories');
    },
	mediaReady_s(){
        return Session.get('mediaready_s');
    },
    noStories(){
        return Session.get('nostories');
    },
    helperClassName: function(counter) {

            var className = counter + 'R';
            return className;
    },
    infoConstructor: function(id , media_type , insta_id) {

            var wholeString = id + '@' + media_type + '@' + insta_id;
            return wholeString;
    }
})
Template.storiestemplate.rendered = function(){

	console.log("getting stories")

    var auxDoc = Graphusers.find({usname : Session.get('username')}).fetch()[0];
	var insta_id = auxDoc.insta_id;

	Meteor.call('StoriesIds',insta_id, function(err , res){

        if (err){
            console.log(err);
        }
        else{
            if (res){

                var counter = 0;
                const numStories = res.data.length;
                
                if (numStories == 0) {
                    Session.set('mediaready_s',true);
                    Session.set('nostories',true);
                }

                for (var j = 0 ; j < numStories ; j++){

                    var container = [];

                    Meteor.call('StoryInfo',res.data[j].id, insta_id, function(err , res){
                        if (err){
                            console.log(err);
                        }
                        else{

                            if(res.result2.media_type == 'VIDEO'){
                                arrayElement = {
                                    imgurl: res.result2.thumbnail_url,
                                    media_type: res.result2.media_type,
                                    id : res.id,
                                    insta_id: insta_id,
                                    since: res.since,
                                    retencion: res.ret,
                                    isvideo: true,
                                    counter: counter
                                }
                            }
                            else{
                                arrayElement = {
                                    imgurl: res.result2.media_url,
                                    media_type: res.result2.media_type,
                                    id : res.id,
                                    insta_id: insta_id,
                                    since: res.since,
                                    retencion: res.ret,
                                    isvideo: false,
                                    counter: counter
                                }
                            }
                            
                            container.push(arrayElement);

                            counter += 1;

                            if (counter == numStories){
                                Session.set('modalstories',container);
                                Session.set('mediaready_s',true);
                            }
                        }
                    });
               }
            } 
            else {
                console.log("result undefined : ",res);
            }
        }
    })
}

Template.storiestemplate.events({
	
	'click .addstory': function(event, instance){

		var counter = event.target.id;
		
		var selector = '.' + counter + 'R';

		var wholeString = $(selector).attr('id');
		console.log(wholeString);
		var infoArray = wholeString.split('@');

		var id = infoArray[0];
		var media_type = infoArray[1];
		var insta_id = infoArray[2];

		var obj = {id : id , insta_id : insta_id , media_type: media_type};

		Session.set('addpoststory', false);

		var campaignname = Session.get('campaignname');

		Meteor.call('AddStory', campaignname , obj);
		
		//document.location.reload(true);
	}
})
