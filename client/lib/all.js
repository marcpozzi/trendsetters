import { Template } from 'meteor/templating';
import { ServiceConfiguration } from 'meteor/service-configuration';
import { Session } from 'meteor/session';
import { Meteor } from 'meteor/meteor';


Template.allusers.helpers({
    allusers: function() {
        return Graphusers.find({} , { sort: { _id : -1} ,limit: 5}).fetch();
    },
    autoSettings: function() {
        return {
          position: "top",
          limit: 5,
          rules: [
            {
              token: '',
              collection: Graphusers,
              field: "usname",
              template: Template.userPill
            }
          ]
        };
    },
    showSearchResult(){
        return Session.get('showsearchresult');
    },
    mostrarTodos(){
        return Session.get('vertodos');
    },
    collectionAll: function(){
        return Graphusers.find({});
    },
    settingsAll: function () {
        return {
            //rowsPerPage: 10,
            showFilter: false,
            showColumnToggles: false,
            fields: [
                { key: 'usname', label: 'Usuario',
                fn: function (value, object, key) { return new Spacebars.SafeString(value+' <a target="_blank" href=https://www.instagram.com/'+value+'/ ><img src="/images/instagram-badge2.png"></a>'); }}, 
                
                { key: 'followers', label: 'Seguidores' , 
                    fn: function (value, object, key) {
                        return value.toLocaleString();}},

                { key: 'reach28', label: 'Alcance 28 dias' , 
                    fn: function (value, object, key) {
                        return value.toLocaleString();}},

                { key: 'impressions28', label: 'Impresiones 28 dias' , 
                    fn: function (value, object, key) {
                        return value.toLocaleString();}},
                    

                { key: 'usname', label: 'Analytics',
                fn: function (value, object, key) { return new Spacebars.SafeString('<img src="/images/analytics-badge2.png" class="getAnalyticsS" id="'+value+'"}}>') }}
            ]
        };
    }
});
Template.allusers.events({
    'click .getAnalytics' : function(event,instance) {
        var analyticsURL = baseURL + 'useranalytics/' + event.target.id;

        window.open(analyticsURL);

    },
    'click .getMedia' : function(event,instance) {
        var usermediaURL = baseURL + 'usermedia/' + event.target.id;

        window.open(usermediaURL);
    },
    'click .getStories' : function(event,instance) {
        var storiesURL = baseURL + 'stories/' + event.target.id;

        window.open(storiesURL);
    },
    'click #vertodos' : function(event,instance) {
        Session.set('vertodos', true);
    },
    'click #ocultartodos' : function(event,instance) {
        Session.set('vertodos', false);
    }
});

Template.searcheduser.events({
    'click .getAnalytics' : function(event,instance) {
        var analyticsURL = baseURL + 'useranalytics/' + event.target.id;

        window.open(analyticsURL);

    },
    'click .getMedia' : function(event,instance) {
        var usermediaURL = baseURL + 'usermedia/' + event.target.id;

        window.open(usermediaURL);
    },
    'click .getStories' : function(event,instance) {
        var storiesURL = baseURL + 'stories/' + event.target.id;

        window.open(storiesURL);
    }
})

Template.all.events({
    'click .nav-link' : function(event,instance) {

        var optionsArray = ["overview", "search", "followhistory","campaigns","trendsetters"];

        const selected = $(event.target.classList);

        optionsArray.forEach(function(element) {
            if (jQuery.inArray(element,selected) == 1){
              Session.set(element,true);
              console.log(element)
            }
            else {
            Session.set(element,false);
            }
        });    
    },
    'click #buscaruser' : function(event, instance) {

        var usname = $('#inputUsuario').val()

        $('#buscaruser').removeClass("btn-primary");
        $('#buscaruser').addClass("btn-secondary");
        $('#buscaruser').prop('disabled', true);

        console.log(usname);

        Template.searcheduser.helpers({
            res: function(){
                return Graphusers.findOne({usname : usname});
            }
        })

        Session.set('showsearchresult',true);
    },
    'click #refreshsearchu' : function(event, instance) {

        $('#inputUsuario').val('');

        $('#buscaruser').removeClass("btn-secondary");
        $('#buscaruser').addClass("btn-primary");
        $('#buscaruser').prop('disabled', false);

        Session.set('showsearchresult',false);
    }
});

Template.all.rendered = function() {
    Session.set('overview',true);
};


Template.all.helpers({
    isOverview(){
        return Session.get('overview');
    },
    isCampaign(){
        return Session.get('campaigns');
    },
    isSearch(){
        return Session.get('search');
    },
    isFollowHistory(){
        return Session.get('followhistory');
    },
    isTrendsetters(){
        return Session.get('trendsetters');
    }

});

