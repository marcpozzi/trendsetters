import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'



Template.useranalytic.helpers({
    userinfofetch: function() {
        var usname = Template.currentData();
        var doc = Graphusers.findOne({usname : usname});

        doc.impressions7 = doc.impressions7.toLocaleString();
        doc.impressions28 = doc.impressions28.toLocaleString();
        doc.reach7 = doc.reach7.toLocaleString();
        doc.reach28 = doc.reach28.toLocaleString();
        doc.followers = doc.followers.toLocaleString();
        doc.follows = doc.follows.toLocaleString();
        doc.profileviews = doc.profileviews.toLocaleString();
        doc.followerscountday = doc.followerscountday.toLocaleString();

        return doc;
    },
    Draw: function(){
        var usname = Template.currentData();
        var doc = Graphusers.findOne({usname : usname});

        DrawGender(doc.male , doc.female);
        DrawAges(doc.ages);
        DrawCities(doc.audiencecity);
        DrawCountries(doc.audiencecountry);
    },
    infoReady: function() {
        var usname = Template.currentData();

        if (Graphusers.findOne({usname : usname}) == undefined) {
            Session.set('infoready', false);
            return Session.get('infoready');
        }
        else{
            Session.set('infoready', true);
            return Session.get('infoready');
        }
    }
});