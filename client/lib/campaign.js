import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
import { Cloudinary } from 'meteor/socialize:cloudinary';


Cloudinary.config({
    cloud_name: 'dpsjdzkg6',
    api_key: '677826898477853'
});


Meteor.subscribe('campaigns');
Campaigns = new Mongo.Collection('campaigns');
Meteor.subscribe('graphusers');
Graphusers = new Mongo.Collection('graphusers');
Meteor.subscribe('images');
Images = new Mongo.Collection('images');

//$.cloudinary.config({
 //   cloud_name:"dpsjdzkg6"
//});


function check_multifile_logo(file) {
    var extension = file.substr((file.lastIndexOf('.') + 1))
    if (extension === 'jpg' || extension === 'jpeg' || extension === 'gif' || extension === 'png' || extension === 'bmp') {
        return true;
    } else {
        return false;
    }
}




// ---------------- CAMPAIGN ----------------------------------------------------

Template.campaign.rendered = function() {
	Session.set('tableready',false);
    Session.set('searchposts',false);
    Session.set('isadmin',false);
    Session.set('postsready',false);
    Session.set('campaigngeneral',true);
    Session.set('postactive',true);
    Session.set('showone',false);
    Session.set('rowid',1000);

    var URL = window.location.pathname;

    Meteor.call('CheckURL', URL, function(err, res){

        Session.set('iscodevalid', res.isvalid);
        Session.set('isadmin', res.isadmin);
        Session.set('isvalidationready', true);
        Session.set('addpoststory', false);
        Session.set('campaignname', res.nombre);
        console.log(Session.get('iscodevalid'));
    })
}



Template.campaign.helpers({
    isValid(){
        return Session.get('iscodevalid');
    },
    isMobile(){
        console.log(Session.get('screensize'));
        return Session.get('screensize');
    },
    isAdmin(){
        return Session.get('isadmin');
    },
    isValidationReady(){
        return Session.get('isvalidationready');
    },
    hashNameHelper(){
        return Session.get('hashedname');
    },
    codeCampaign(){
        return Session.get('campaignname');
    },
    postActive(){
        return Session.get('postactive');
    },
    addPostStory(){
        return Session.get('addpoststory');
    },
    searchPosts(){
        return Session.get('searchposts');
    },
    submitImage(){
        return Session.get('submitimage');
    },
    postsReady(){
        return Session.get('postsready');
    },
    showOne(){
        console.log("Show one",Session.get('showone'));
        return Session.get('showone');
    },
    tableReadyG(){

        var campaignname = Session.get('campaignname');

        if (Campaigns.findOne({nombre : campaignname}) == undefined) {
            Session.set('tableready', false);
            return Session.get('tableready');
        }
        else{
            Session.set('tableready', true);
            return Session.get('tableready');
        }
    },
    campaigninfo: function() {

            var campaignname = Session.get('campaignname');
            return Campaigns.findOne({nombre : campaignname});
    },
    nroPosteosTotal: function() {

        var campaignname = Session.get('campaignname');
        var doc = Campaigns.findOne({nombre : campaignname});
        return doc.posts.length;
    },
    trendsetters: function() {
        return Graphusers.find({trendsetter: 1});
    },
    tableReady(){

        var campaignname = Session.get('campaignname');

        if (Campaigns.findOne({nombre : campaignname}) == undefined) {
            Session.set('tableready', false);
            return Session.get('tableready');
        }
        else{
            Session.set('tableready', true);
            return Session.get('tableready');
        }
    },
    postsdata: function() {
        var campaignname = Session.get('campaignname');

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        var doc = Campaigns.findOne({nombre : campaignname });

        if (doc == undefined){
            return [{
                    impressions: null,
                    reach: null,
                    engagement: null,
                    saved: null,
                    video_views: null,
                    like_count: null,
                    comments_count: null,
                    media_type: null,
                    id: null,
                    usname: "Loading data",
                    followers: null,
                }]
        }
        else{
            return doc.posts;
        }
    },
    OnePost: function(id){
        var campaignname = Session.get('campaignname');

        var doc = Campaigns.findOne({nombre : campaignname });

        for (var i = 0 ; i < doc.posts.length ; i++){
            if(doc.posts[i].id == id){

                var result = [];

                if (doc.posts[i].media_type == 'S') doc.posts[i].isstory = true;
                else doc.posts[i].isstory = false;
                if (doc.posts[i].media_type == 'I') doc.posts[i].isfoto = true;
                else doc.posts[i].isfoto = false;
                if (doc.posts[i].media_type == 'V') doc.posts[i].isvideo = true;
                else doc.posts[i].isvideo = false;

                if (doc.posts[i].engagement == null) {
                    var engRate = '-';
                }
                else {
                    var engRate = doc.posts[i].engagement * 100 / doc.posts[i].reach;
                    engRate = Number(engRate.toFixed(1));
                    engRate = engRate.toLocaleString();
                    doc.posts[i].engRate = engRate;
                }
                if (doc.posts[i].media_type == 'V') {
                    var viewRate = doc.posts[i].video_views * 100 / doc.posts[i].reach;
                    viewRate = Number(viewRate.toFixed(1));
                    viewRate = viewRate.toLocaleString();
                    doc.posts[i].viewRate = viewRate;
                }

                result.push(doc.posts[i]);
                return result;
            }
        }
    },
    ManualTable(){
        var campaignname = Session.get('campaignname');

        var doc = Campaigns.findOne({nombre : campaignname });
        var posts = [];

        for (var i = 0 ; i < doc.posts.length ; i++){
            if (doc.posts[i].media_type == 'S') var isstory = true;
            else isstory = false;
            if (doc.posts[i].media_type == 'I') var isfoto = true;
            else isfoto = false;
            if (doc.posts[i].media_type == 'V') var isvideo = true;
            else isvideo = false;


            var impressions = doc.posts[i].impressions;
            
            var followers = doc.posts[i].followers;
            var engagement = doc.posts[i].engagement;
            var reach = doc.posts[i].reach;


            
            impressions = impressions.toLocaleString();
            followers = followers.toLocaleString();
            reach = reach.toLocaleString();
            
            


            if (doc.posts[i].media_type == 'V') {
                var views = doc.posts[i].video_views;
                views = views.toLocaleString();
                var viewRate = doc.posts[i].video_views * 100 / doc.posts[i].reach;
                viewRate = Number(viewRate.toFixed(1));
            }
            else {
                var views = '-';
                var viewRate = '-';
            }

            if (doc.posts[i].engagement == null) {
                var engRate = '-';
            }
            else {
                var engRate = doc.posts[i].engagement * 100 / doc.posts[i].reach;
                engRate = Number(engRate.toFixed(1));
                engRate = engRate.toLocaleString();

                var like_count = doc.posts[i].like_count;
                var comments_count = doc.posts[i].comments_count;
                var saved = doc.posts[i].saved;

                engagement = engagement.toLocaleString();
                like_count = like_count.toLocaleString();
                comments_count = comments_count.toLocaleString();
                saved = saved.toLocaleString();
            }

            if (doc.posts[i].media_type == 'S') {
                var taps_forward = doc.posts[i].taps_forward;
                var taps_back = doc.posts[i].taps_back;
                var replies = doc.posts[i].replies;
                var exits = doc.posts[i].exits;
            }
            else{
                var taps_forward = '-';
                var taps_back = '-';
                var replies = '-';
                var exits = '-';
            }

            

            var element = {
                isvideo: isvideo,
                isstory: isstory,
                isfoto: isfoto,
                replies: replies,
                exits: exits,
                id: doc.posts[i].id,
                impressions: impressions,
                reach: reach,
                usname: doc.posts[i].usname,
                engagement: engagement,
                views: views,
                followers: followers,
                viewRate: viewRate,
                engRate: engRate,
                like_count: like_count,
                comments_count: comments_count,
                saved: saved,
                media_url: doc.posts[i].media_url
            }
            posts.push(element);
        }
        return posts;
    },
    settingsAC: function() {
        return {
          position: "top",
          limit: 5,
          rules: [
            {
              token: '',
              collection: Graphusers,
              field: "usname",
              template: Template.userPill
            }
          ]
        };
    },

    photoPerformance: function() {
        var campaignname = Session.get('campaignname');

        var doc = Campaigns.findOne({nombre : campaignname });

        var cumRate = 0;
        var counter = 0;
        var impactos = 0;
        var impresiones = 0;

        for ( var i = 0 ; i < doc.posts.length ; i++) {

            if (doc.posts[i].media_type == 'I'){
                cumRate += doc.posts[i].engagement / doc.posts[i].reach;
                counter += 1;
                impactos += doc.posts[i].reach;
                impresiones += doc.posts[i].impressions;
            }
        }
        if (counter == 0){
            var obj = {
                engRate: "No hay fotos",
                impactos: "-",
                impresiones: "-"
            };
        }
        else{
            var obj = {
                engRate: (cumRate/ counter * 100).toFixed(2),
                impactos: impactos.toLocaleString(),
                impresiones: impresiones.toLocaleString()
            };
        }
        return obj;
    },
    videoPerformance: function() {
        var campaignname = Session.get('campaignname');

        var doc = Campaigns.findOne({nombre : campaignname });

        var cumRateV = 0;
        var cumRateVE = 0;
        var counter = 0;
        var impactos = 0;
        var views = 0;

        for ( var i = 0 ; i < doc.posts.length ; i++) {

            if (doc.posts[i].media_type == 'V'){
                cumRateVE += doc.posts[i].engagement / doc.posts[i].reach;
                cumRateV += doc.posts[i].video_views / doc.posts[i].reach;
                counter += 1;
                impactos += doc.posts[i].reach;
                views += doc.posts[i].video_views;
            }
        }
        if (counter == 0){
            var obj = {
                viewRate: "-",
                impactos: "-",
                views: "-",
                engRate: "No hay videos"
            };
        }
        else{
            var obj = {
                viewRate: (cumRateV/ counter * 100).toFixed(2),
                engRate: (cumRateVE/ counter * 100).toFixed(2),
                impactos: impactos.toLocaleString(),
                views: views.toLocaleString()
            };
        }

        return obj;
    },
    storyPerformance: function() {
        var campaignname = Session.get('campaignname');

        var doc = Campaigns.findOne({nombre : campaignname });

        var counter = 0;
        var impactos = 0;
        var impresiones = 0;

        for ( var i = 0 ; i < doc.posts.length ; i++) {

            if (doc.posts[i].media_type == 'S'){
                counter += 1;
                impactos += doc.posts[i].reach;
                impresiones += doc.posts[i].impressions;
            }
        }
        if (counter == 0){
            var obj = {
                impactos: "-",
                impresiones: "No hay stories"
            };
        }
        else{
            var obj = {
                impactos: impactos.toLocaleString(),
                impresiones: impresiones.toLocaleString()
            };
        }
        
        return obj;
    },
    campaignImages: function() {

        var campaignname = Session.get('campaignname');

        //console.log(Images.find({campaignname: campaignname}))
        return Images.find({campaignname: campaignname});
    }
})


Template.campaign.events({

    'change #filecloud' : function(event,instance) {

        var campaignname = Session.get('campaignname');
        
        const uploads = Cloudinary.uploadFiles(event.currentTarget.files);

        uploads.forEach(async (response) => {
            const photoData = await response;
            //console.log("login data" , photoData);

            Session.set('submitimage',true);

            Meteor.call('saveImage', photoData , campaignname);
        });


        /*
        uploads.forEach(async (response) => {
            const photoData = await response;
            new Photo(photoData).save();
        }); */
    },

    'change #fileup' : function(event,instance) {
        
           var preview = document.querySelector('img'); //selects the query named img
           var file    = document.querySelector('input[type=file]').files[0]; //sames as here
           var reader  = new FileReader();
           var campaignname = Session.get('campaignname');

           reader.onloadend = function () {
               preview.src = reader.result;

               Meteor.call('addImage', {data:reader.result , name:'file1' , campaignname: campaignname });
           }

           if (file) {
               reader.readAsDataURL(file); //reads the data as a URL
           } else {
               preview.src = "";
           }
    },
    'click #submitimagen' : function(event,instance) {
        Session.set('submitimagen',false);
        Session.set('addpoststory',false);
    },
    'click #addpoststory' : function(event,instance) {
        Session.set('addpoststory',true);
    },
    'click #cancelarpost' : function(event,instance) {
        Session.set('addpoststory',false);
    },
    'click #buscarpost' : function(event,instance) {

        var usname = $('#inputUsername').val();
        Session.set('username',usname);
        Session.set('mediaready',false);
        Session.set('mediaready_s',false);
        Session.set('searchposts',true);
    },
    'click #storyoption': function(){       
        $('#postoption').removeClass('isActive').addClass('notActive');
        $('#storyoption').removeClass('notActive').addClass('isActive');
        Session.set('postactive',false);
    },
    'click #postoption': function(){        
        $('#storyoption').removeClass('isActive').addClass('notActive');
        $('#postoption').removeClass('notActive').addClass('isActive');
        Session.set('postactive',true);
    },
    'click .removeRow': function(e , instance) {
        
        var campaignname = Session.get('campaignname');

        Meteor.call('RemoveMedia', campaignname , e.target.id);

        //document.location.reload(true);
    },
    'click .editRow': function(e , instance) {
        
        var campaignname = Session.get('campaignname');
        var id = event.target.id;

        Session.set('rowid',id);

    },
    'click #agregarmanual': function(e, instance) {
        $('#manualmodalid').modal('show');
    },
    'click #exportpdf': function(e, i) {
        console.log("clicked");

        Meteor.call('ExportPDF');

    },
    'touchend .mobile-img': function(e, instance) {
        var id = e.target.id;

        console.log(id);
        Session.set('showone', id);
    },
    'click #volverposts': function(e, instance) {

        Session.set('showone', false);
    }
})


Template.reactiveTable.events({
    'click .getAnalytics' : function(event,instance) {
        var analyticsURL = baseURL + 'useranalytics/' + event.target.id;

        window.open(analyticsURL);
    }
});




