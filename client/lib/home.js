import { Template } from 'meteor/templating';
import { ServiceConfiguration } from 'meteor/service-configuration';
import { Session } from 'meteor/session'
import { Meteor } from 'meteor/meteor'




Template.home.rendered = function() {
    Session.set('isloaded',false);
};

Template.home.events({
    'click #Mamut' : function(event, instance){

        var name = event.target.id;
        var campaignURL = '/campaign/' + name;

        Meteor.call('Name2Hash', campaignURL, function(err, res){

            var campaignURL = baseURL + 'campaign/' + res;
            console.log(res);
        	window.open(campaignURL);
        })
    },
    'click #login-buttons-facebook' : function(event , instance){
        event.preventDefault();

        Meteor.loginWithFacebook({requestPermissions: ['public_profile', 'email','instagram_basic', 'pages_show_list', 'manage_pages','instagram_manage_insights']}, function(err){
            if (err) {
                console.log('Handle errors here: ', err);
            }
        });
    },
    'click #login-buttons-logout' : function(event , instance){
        event.preventDefault();

        Meteor.logout();
    }
});

Template.home.helpers({
    isLoaded(){
        return Session.get('isloaded');
    },
    getIpAddress(){
        Meteor.call('returnIp', function(err , res) {
            Session.set('ipaddress' , res);
        })
    },
    returnIpAddress(){
        return Session.get('ipaddress');
    },
    trendUsers: function() {
        //console.log(Graphusers.find({trendsetter: 1}).sort({prioridad: 1}))
        //console.log(Graphusers.find({trendsetter: 1}))
            return Graphusers.find({trendsetter: 1} , {sort:{prioridad:1}});
    }
});

Accounts.ui.config({ requestPermissions: { facebook: ['public_profile', 'email','instagram_basic', 'pages_show_list', 'manage_pages','instagram_manage_insights'] } });

Template.loginButtons.events({
    'click .login-facebook': function(e) {//login-facebook
        e.preventDefault();

        Meteor.loginWithFacebook({requestPermissions: ['public_profile', 'email','instagram_basic', 'pages_show_list', 'manage_pages','instagram_manage_insights']}, function(err){
            if (err) {
                console.log('Handle errors here: ', err);
            }
        });
    }
});

Accounts.onLogin(function(){
  Meteor.call('ChainedCall', function(err , res){
    if (err) console.log(err);
    else {
        Session.set('isloaded',true);
        console.log("all ok");
    }
    });
});