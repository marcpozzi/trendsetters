import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';



Template.poststemplate.helpers({
	modalPosts(){
		//var posts = Session.get('modalposts');
		//console.log(posts);
        return Session.get('modalposts');
    },
    mediaReady(){
        return Session.get('mediaready');
    },
    morePosts(){
        return Session.get('moreposts');
    },
    helperClassName: function(counter) {

            var className = counter + 'R';
            return className;
    },
    infoConstructor: function(id , media_type , insta_id) {

            var wholeString = id + '@' + media_type + '@' + insta_id;
            return wholeString;
    }
})
Template.poststemplate.rendered = function(){

	    var auxDoc = Graphusers.find({usname : Session.get('username')}).fetch()[0];
		var insta_id = auxDoc.insta_id;
		var range = [0 , 5];
		
		
		Meteor.call('MediaIds',insta_id , range , function(err , res){

	        if (err){
	            console.log(err);
	        }
	        else{
	            if (res){

	                var counter = 0;
	                const numPosts = res.length;

	                for (var j = 0 ; j < numPosts ; j++){

	                	var ColContainer = [];
	                	var RowContainer = [];

	                    Meteor.call('MediaInfo',res[j].id, insta_id, function(err , res){
	                    	if (err) { 
	                    		console.log(err);
	                    	}
	                    	else{
	                    		//console.log(res.result1.media_type);

	                    		if(res.result1.media_type == 'VIDEO'){
	                    			arrayElement = {
	                                    imgurl: res.result1.thumbnail_url,
	                                    media_type: res.result1.media_type,
	                                    id : res.id,
	                                    insta_id: insta_id,
	                                    isvideo: true,
	                                    isalbum: false,
	                                    counter: counter
	                                }
	                    		}
	                    		else if (res.result1.media_type == 'IMAGE'){
	                    			arrayElement = {
	                                    imgurl: res.result1.media_url,
	                                    media_type: res.result1.media_type,
	                                    id : res.id,
	                                    insta_id: insta_id,
	                                    isvideo: false,
	                                    isalbum: false,
	                                    counter: counter
	                                }
	                    		} else {
	                    			arrayElement = {
	                                    imgurl: res.result1.media_url,
	                                    media_type: res.result1.media_type,
	                                    id : res.id,
	                                    insta_id: insta_id,
	                                    isvideo: false,
	                                    isalbum: true,
	                                    counter: counter
	                                }
	                    		}
	                    		RowContainer.push(arrayElement);

	                        	counter += 1;

	                        	if (counter % 5 == 0){
	                                ColContainer.push(RowContainer);
	                                RowContainer = [];
	                            }

	                        	if (counter == numPosts){
	                                Session.set('modalposts', ColContainer);
	                                Session.set('mediaready', true);
	                                Session.set('numposts', 5);
	                            }

	                    	}
	                    })
	                }
	            }
			}
		})
}

Template.poststemplate.events({
	'click #moreresults': function(event, instance) {


		var auxDoc = Graphusers.find({usname : Session.get('username')}).fetch()[0];
		var insta_id = auxDoc.insta_id;
		var range = [Session.get('numposts') , Session.get('numposts') + 5];
		Session.set('moreposts', true);

		
		Meteor.call('MediaIds',insta_id , range , function(err , res){

	        if (err){
	            console.log(err);
	        }
	        else{
	            if (res){

	                var counter = Session.get('numposts');
	                const numPosts = res.length;

	                for (var j = 0 ; j < numPosts ; j++){

	                	var RowContainer = [];

	                    Meteor.call('MediaInfo',res[j].id, insta_id, function(err , res){
	                    	if (err) { 
	                    		console.log(err);
	                    	}
	                    	else{
	                    		//console.log(res.result1.media_type);

	                    		if(res.result1.media_type == 'VIDEO'){
	                    			arrayElement = {
	                                    imgurl: res.result1.thumbnail_url,
	                                    media_type: res.result1.media_type,
	                                    id : res.id,
	                                    insta_id: insta_id,
	                                    isvideo: true,
	                                    isalbum: false,
	                                    counter: counter
	                                }
	                    		}
	                    		else if (res.result1.media_type == 'IMAGE'){
	                    			arrayElement = {
	                                    imgurl: res.result1.media_url,
	                                    media_type: res.result1.media_type,
	                                    id : res.id,
	                                    insta_id: insta_id,
	                                    isvideo: false,
	                                    isalbum: false,
	                                    counter: counter
	                                }
	                    		} else {
	                    			arrayElement = {
	                                    imgurl: res.result1.media_url,
	                                    media_type: res.result1.media_type,
	                                    id : res.id,
	                                    insta_id: insta_id,
	                                    isvideo: false,
	                                    isalbum: true,
	                                    counter: counter
	                                }
	                    		}
	                    		RowContainer.push(arrayElement);

	                        	counter += 1;


	                        	if (counter == Session.get('numposts') + 5){
	                        		var ColContainer = Session.get('modalposts');
	                        		ColContainer.push(RowContainer);

	                        		const totalPosts = Session.get('numposts')
	                        		Session.set('numposts', totalPosts + 5);
	                        		Session.set('moreposts', false);

	                                Session.set('modalposts',ColContainer);
	                            }

	                    	}
	                    })
	                }
	            }
			}
		})
	},
	'click .addpost': function(event, instance){

		var counter = event.target.id;
		//var counter = $(this).attr('id');

		//console.log(counter);
		
		var selector = '.' + counter + 'R';

		//console.log(selector);

		var wholeString = $(selector).attr('id');
		console.log(wholeString);
		var infoArray = wholeString.split('@');

		var id = infoArray[0];
		var media_type = infoArray[1];
		var insta_id = infoArray[2];

		//console.log(media_type);
		//console.log(insta_id);
		//console.log(id);

		var obj = {id : id , insta_id : insta_id , media_type: media_type};

		Session.set('addpoststory', false);

		var campaignname = Session.get('campaignname');

		Meteor.call('AddPost', campaignname , obj);
		
		//document.location.reload(true);
	}
})
