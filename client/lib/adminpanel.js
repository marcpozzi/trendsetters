import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'
import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'




Accounts.config({
  forbidClientAccountCreation: false,
});



Template.adminpanel.rendered = function(){
    Session.set('isaddcampaign', false);
    Session.set('isadmin', false);

    $('.carousel').carousel({ interval: 100000 });
}

Template.adminpanel.helpers({
    CampaignSettings: function () {
        return {
            rowsPerPage: 20,
            showFilter: false,
            showColumnToggles: false,
            showNavigation: 'never',
            showNavigationRowsPerPage: false,
            fields: [
                { key: 'nombre', label: 'Nombre'},
                { key: 'cliente', label: 'Cliente'},
                { key: 'central', label: 'Central'},
                 { key: 'fecha', label: 'Fecha'},
                 { key: 'nombre', label: 'Link',
                    fn: function (value, object, key) { 

                            return new Spacebars.SafeString('<img src="/images/arrow-icon.png" class="gotocampaignhash" id="'+value+'"}}>'); }},
                { key: 'nombre', label: 'Admin Link',
                    fn: function (value, object, key) { 

                            return new Spacebars.SafeString('<img src="/images/arrow-icon.png" class="gotocampaignhash" id="@'+value+'"}}>'); }}
            ]
        };
    },
    CampaignCollection: function () {
        return Campaigns.find({});
    },
    CampaignCollection2: function () {

        var docs = Campaigns.find({}).fetch();

        var row = [];
        var cols = [];

        var Multiplier = 1;

        for(var i = 0 ; i < docs.length ; i++){
            if (i < Multiplier * 4) {
                row.push(docs[i]);
            }
            if (i == Multiplier * 4 ){
                cols.push(row);
                var row = [];
                row.push(docs[i]);
                Multiplier += 1;
            }
            if (i == (docs.length - 1) ){
                cols.push(row);
            }
        }

        return cols;
    },
    isAddCampaign: function() {
        return Session.get('isaddcampaign');
    },
    verPerfiles: function() {
        return Session.get('isverperfiles');
    },
    removeDot: function(string){
        return string.replace('.',"");
    },
    checkUser: function(){

        if(Meteor.user() == null || Meteor.user() == undefined){
            console.log("Loading user or no user");
            var email = "-";
        }
        else{
            var email = Meteor.user().emails[0].address;
        }

        if (email == 'mpozzi@trendsetters.com' || email == 'pr@trendsetters.com' || email == 'prensa@trendsetters.com' ){
            Session.set('isadminc', true);
            console.log("User verified is admin");
        }
    },
    isAdminC: function() {
        return Session.get('isadminc');
    },
    addArroba: function(string){
        return '@'+string;
    },
    trendUsers: function() {
        return Graphusers.find({trendsetter: 1});
    }
})

Template.adminpanel.events({
    'click #addcampaign' : function(event,instance) {
        Session.set('isaddcampaign', true);
    },
    'click #verperfiles' : function(event,instance) {
        Session.set('isverperfiles', true);
    },
    'click #noverperfiles' : function(event,instance) {
        Session.set('isverperfiles', false);
    },
    'click #cancelarcampaign' : function(event,instance) {
        Session.set('isaddcampaign', false);
    },
    'submit .form-signin'(event, instance) {

        event.preventDefault();
        Session.set('isaddcampaign', false);

        var nombre = event.target.inputName.value.replace(/\s/g, "");

        var obj = {
            nombre: nombre,
            cliente: event.target.inputCliente.value,
            fecha: event.target.campaigndate.value,
            central: event.target.inputCentral.value
        }

        Meteor.call('CreateCampaign', obj );
    },
    'click #mlogin' : function(event,instance) {
        //console.log("validating log in");

        var email = $('#inputuname').val();
        var Passwrd = $('#inputpass').val();

        Meteor.loginWithPassword(email, Passwrd);

    },
    'click #logout' : function(event,instance) {

        Meteor.logout();

    },
    
    'click .gotocampaignhash' : function(event, instance){

        var campaignname = event.target.id
        var hash;

        Meteor.call('CryptrName2Hash',campaignname , function(err,res){
            hash = res;

            var campaignURL = baseURL + 'campaign/' + hash;

            window.location.href = campaignURL;
        })
    },
    'click .goAnalytics' : function(event, instance){
        
        var analyticsURL = baseURL + 'useranalytics/' + event.target.id;

        window.open(analyticsURL);
    }
})
        



Template.reactiveTable.events({
    'click .gotocampaignhash' : function(event, instance){

        var campaignname = event.target.id
        var hash;

        Meteor.call('CryptrName2Hash',campaignname , function(err,res){
            hash = res;

            var campaignURL = baseURL + 'campaign/' + hash;

            window.open(campaignURL);
        })
    }
})



