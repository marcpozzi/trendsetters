import { Meteor } from 'meteor/meteor'
import fbgraph from 'fbgraph';
import future from 'fibers/future';




Meteor.methods({
    GetAccounts:  function(){

        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var NumAccounts = 0;
        var ids = [];
        var names = [];

        if(Meteor.user().services.facebook.accessToken) {
            graph.setVersion("2.10");
            graph.setAccessToken(Meteor.user().services.facebook.accessToken);

            var future = new Future();
            var onComplete = future.resolver();
            
            graph.get('me/accounts',function(err,result) { // ID: 10211775023024257

                if (err){
                    console.log("Error in getting accounts");
                    console.log(err);
                    onComplete(err,result);
                }
                else{
                    NumAccounts = result.data.length;

                    if (NumAccounts==0) console.log("No pages in this account")

                    for (i = 0; i < result.data.length; i++) {
                    ids[i] = result.data[i].id;
                    names[i] = result.data[i].name;
                    }

                    onComplete(err,result);
                }
                
                
            });
        }

        Future.wait(future);
        return {ids: ids , names: names };
    },

    IsInstaConnected:  function(account_id){
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var IsConnected = false;

        if(Meteor.user().services.facebook.accessToken) {
            graph.setVersion("2.10");
            graph.setAccessToken(Meteor.user().services.facebook.accessToken);

            //console.log("used token is ",Meteor.user().services.facebook.accessToken);

            var future = new Future();
            var onComplete = future.resolver();
            var req0 = account_id + '?fields=instagram_business_account';
                
            graph.get(req0,function(err,result) { 
                try{
                    //console.log("---- checking for page id : ", account_id);
                    //console.log("---------------- ERR IS --------------------------");
                    //console.log(err);
                    //console.log("---------------- RESULT IS --------------------------");
                    //console.log(result);
                	insta_id = result.instagram_business_account.id;
                	IsConnected = insta_id;
                }
                catch(error){
                    //console.log("---------------- CATCHED --------------------------");
                    //console.log(error);
                	IsConnected = false;
                }
                
                onComplete(err,result);
            });
        }
        Future.wait(future);
        return IsConnected;
    },

    SubInfoRequest:  function(insta_id) {
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');


        if(Meteor.user().services.facebook.accessToken) {
            graph.setVersion("2.10");
            graph.setAccessToken(Meteor.user().services.facebook.accessToken);

            var future11 = new Future();
            var onComplete11 = future11.resolver();
            var future12 = new Future();
            var onComplete12 = future12.resolver();
            var accToken = Meteor.user().services.facebook.accessToken;
            var emailAddress = Meteor.user().services.facebook.email;
            var fbname = Meteor.user().services.facebook.fbname;

            
            var req1 = insta_id + '?fields=username,followers_count,follows_count,media_count,profile_picture_url,biography';
            var req2 = insta_id + '/insights?metric=impressions,reach&period=week';
            var req3 = insta_id + '/insights?metric=impressions,reach&period=days_28';
            var req4 = insta_id + '/insights?metric=audience_gender_age&period=lifetime';
            var req5 = insta_id + '/insights?metric=profile_views,follower_count,email_contacts&period=day';
            var req6 = insta_id + '/insights?metric=audience_locale,audience_country,audience_city&period=lifetime';


            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                },
                {
                    method: "GET",
                    relative_url: req2// Get insights, impressions, reach WEEK timeframe
                },
                {
                    method: "GET",
                    relative_url: req3// Get insights, impressions, reach DAYS_28 timeframe
                },
                {
                    method: "GET",
                    relative_url: req4 // Get audience gender and age
                },
                {
                    method: "GET",
                    relative_url: req5 // Get views and contacts
                },
                {
                    method: "GET",
                    relative_url: req6 // Get views and contacts
                }
            ],function(err, res) {

                // Discovering results
                var result1 = JSON.parse(res[0].body);
                var result2 = JSON.parse(res[1].body);
                var result3 = JSON.parse(res[2].body);
                var result4 = JSON.parse(res[3].body);
                var result5 = JSON.parse(res[4].body);
                var result6 = JSON.parse(res[5].body);

                console.log("\ngetting account info of , ", result1.username);

                var genderAndAges = rewriteProperties(result4.data[0].values[0].value);
                var resultInfo = processInfo(result4.data[0].values[0].value);
                var maxIndex = findMaxIndex(resultInfo.ages);
                const link = "https://www.instagram.com/"+result1.username+"/";
                var creationDate = Date.now();
                var firstfollowers = Number(result1.followers_count);

               
                Graphusers.rawCollection().findOne({usname:result1.username}, function(err,res){

                    if (!res){
                        Graphusers.rawCollection().insert({
                            accesstoken:accToken,
                            fbname: fbname,
                            email: emailAddress,
                            link: link,
                            insta_id: insta_id,
                            picurl: result1.profile_picture_url,
                            bio: result1.biography,
                            usname: result1.username,
                            followers: result1.followers_count,
                            followers_string: result1.followers_count.toLocaleString(),
                            follows: result1.follows_count,
                            numposts: result1.media_count,
                            locale: result6.data[0].values[0].value,
                            audiencecountry: result6.data[1].values[0].value,
                            gender_ages: genderAndAges,
                            impressions7: result2.data[0].values[0].value,
                            reach7: result2.data[1].values[0].value,
                            impressions28: result3.data[0].values[0].value,
                            reach28: result3.data[1].values[0].value,
                            profileviews: result5.data[0].values[0].value,
                            emailcontacts: result5.data[2].values[0].value,
                            followerscountday: result5.data[1].values[0].value,
                            ages: resultInfo.ages,
                            firstdate: creationDate,
                            agegroup: maxIndex,
                            female: resultInfo.female,
                            male: resultInfo.male,
                            maleRatio: resultInfo.male/(resultInfo.male + resultInfo.female),
                            followershistory: [firstfollowers]

                        }, (error, result) => {
                            if (error) {
                                console.log("Error inserting document to collection")
                            }
                            else {
                                console.log("Document inserted in collection");
                            }
                        })
                    }
                    else{
                        console.log("User already in database");
                    }
                    onComplete12(err, res);
                });
                onComplete11(err, res);
            });


            //return future;
            Future.wait(future11);
            Future.wait(future12);
            return true;
            }
        else{
            return false;
        }
    }
    
});