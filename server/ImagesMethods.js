import { Meteor } from 'meteor/meteor'
import fbgraph from 'fbgraph';
import future from 'fibers/future';
import { Cloudinary } from 'meteor/socialize:cloudinary';


Images = new Mongo.Collection('images');


Cloudinary.config({
    cloud_name: 'dpsjdzkg6',
    api_key: '677826898477853',
    api_secret: 'fIm0ZnREfjJlW_VzJKU2m4GmsdE'
});


// Rules are bound to the connection from which they are are executed. This means you have a userId available as this.userId if there is a logged in user. Throw a new Meteor.Error to stop the method from executing and propagate the error to the client. If rule is not set a standard error will be thrown.
Cloudinary.rules.delete = function (publicId) {
	if (!this.userId && !publicId) throw new Meteor.Error("Not Authorized", "Sorry, you can't do that!");
};

Cloudinary.rules.sign_upload = function () {
	if (!this.userId) throw new Meteor.Error("Not Authorized", "Sorry, you can't do that!")
};

Cloudinary.rules.private_resource = function (publicId) {
	if (!this.userId && !publicId) throw new Meteor.Error("Not Authorized", "Sorry, you can't do that!");
};

Cloudinary.rules.download_url = function (publicId) {
	if (!this.userId && !publicId) throw new Meteor.Error("Not Authorized", "Sorry, you can't do that!");
};

Meteor.publish('images',function(){
  return Images.find({});
});




Meteor.methods({
    saveImage:function(obj , campaignname){

        console.log("Inserting image to campaign : " , campaignname);
        console.log(obj.url);

        obj.campaignname = campaignname;

        Images.insert({
            public_id: obj.public_id,
            campaignname: campaignname,
            format: obj.format,
            url: obj.url,
            secure_url: obj.secure_url,
            obj: obj
        });


    }
});