import { Meteor } from 'meteor/meteor'
import fbgraph from 'fbgraph';
import future from 'fibers/future';



Campaigns = new Mongo.Collection('campaigns');

Meteor.publish('campaigns',function(){
  return Campaigns.find({});
});

Graphusers = new Mongo.Collection('graphusers');

Meteor.publish('graphusers',function(){
  return Graphusers.find({}, {
    fields: { accessToken: 0 }
  });
});

function getSecondPart(str) {
    return str.split('/')[1];
}

function getLastPart(str) {
    var strArray = str.split('/');
    return strArray[strArray.length-1];
}

Meteor.methods({
    CheckURL:  function(URL){

        var Cryptr = require('cryptr');
        var cryptr = new Cryptr('thinky2017');

        var docs = Campaigns.find({}).fetch();

        //console.log("checking url validity");
        var result = false;
        var isadmin = false;
        var campaignname;

        var code = getLastPart(URL);

        for (var i = 0 ; i < docs.length ; i++){

            name = cryptr.decrypt(code);

            if (name.charAt(0) == '@') {
                name = name.substr(1);
                isadmin = true;
            }

            if (docs[i].nombre == name) {
                campaignname = docs[i].nombre;
                result = true;
                break;
            }
        }

        var container = {isvalid: result, isadmin: isadmin ,nombre: campaignname};
        console.log("URL check gave : \n", container);

        return container;
    },
    CryptrName2Hash: function(campaignname){

        var Cryptr = require('cryptr');
        var cryptr = new Cryptr('thinky2017');

        //console.log("Encrypting - ",campaignname);

        var encryptedString = cryptr.encrypt(campaignname);
        var decryptedString = cryptr.decrypt(encryptedString);
     
        //console.log("Result - ",encryptedString); 

        return encryptedString;
    },
    returnIp: function() {
      return this.connection.clientAddress;
    }
});