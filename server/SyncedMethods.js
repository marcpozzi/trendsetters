import { Meteor } from 'meteor/meteor'
import later from 'later';



SyncedCron.add({

  name: 'Seguimiento de followers',

  schedule: function() {
  	var later = Npm.require('later');
    return later.parse.recur().every(24).hour();
  },

  job: function() {

    Graphusers.find({}).forEach( function(doc) {

        var insta_id = doc.insta_id;

        Meteor.call('UpdateUserDataComplete', insta_id ,function(err,res) {
            
            if (err){
                console.log(err);
            }
        })

    })
    
    return true;
  }
});



Meteor.methods({
    UpdateUserData:  function(insta_id){

        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var doc = Graphusers.findOne({insta_id: insta_id});
        var user_name = doc.usname;

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            var future = new Future();
            var onComplete = future.resolver();
        
            var req1 = insta_id + '?fields=username,followers_count,follows_count,media_count,profile_picture_url,biography';
            var req2 = insta_id + '/insights?metric=impressions,reach&period=week';
            var req3 = insta_id + '/insights?metric=impressions,reach&period=days_28';
            var req4 = insta_id + '/insights?metric=audience_gender_age&period=lifetime';
            var req5 = insta_id + '/insights?metric=profile_views,follower_count,email_contacts&period=day';
            var req6 = insta_id + '/insights?metric=audience_locale,audience_country,audience_city&period=lifetime';

            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                },
                {
                    method: "GET",
                    relative_url: req2
                },
                {
                    method: "GET",
                    relative_url: req3
                },
                {
                    method: "GET",
                    relative_url: req4 
                },
                {
                    method: "GET",
                    relative_url: req5
                },
                {
                    method: "GET",
                    relative_url: req6 
                }
            ],function(err, res) {

                try {
                    if(res.error){
                        console.log("Error updating ",user_name,"'s data");
                        console.log(res.error.message);
                        onComplete(err,res);
                    }
                    else{

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);
                        var result3 = JSON.parse(res[2].body);
                        var result4 = JSON.parse(res[3].body);
                        var result5 = JSON.parse(res[4].body);
                        var result6 = JSON.parse(res[5].body);

                        if ( result1.error || result2.error || result3.error || result4.error || result5.error || result6.error){
                            console.log("Error updating ",user_name,"'s data");
                            console.log("Result 1 error was ", result1.error.message);
                            onComplete(err,res);
                        }
                        else{

                            var genderAndAges = rewriteProperties(result4.data[0].values[0].value);
                            var audiencecity = removeDotsCities(result6.data[2].values[0].value);
                            var resultInfo = processInfo(result4.data[0].values[0].value);
                            var maxIndex = findMaxIndex(resultInfo.ages);
                            const link = "https://www.instagram.com/"+result1.username+"/";

                           
                            Graphusers.rawCollection().findOne({insta_id: insta_id}, function(err,res){

                                if (res){
                                    Graphusers.rawCollection().update({insta_id : insta_id},{$push: {followershistory: {time: Date.now(), followers: result1.followers_count} }, 
                                        $set: {
                                            usname: result1.username,
                                            link: link,
                                            followers: result1.followers_count,
                                            followers_string: result1.followers_count.toLocaleString(),
                                            follows: result1.follows_count,
                                            numposts: result1.media_count,
                                            picurl: result1.profile_picture_url,
                                            locale: result6.data[0].values[0].value,
                                            audiencecountry: result6.data[1].values[0].value,
                                            audiencecity: audiencecity,
                                            gender_ages: genderAndAges,
                                            impressions7: result2.data[0].values[0].value,
                                            reach7: result2.data[1].values[0].value,
                                            impressions28: result3.data[0].values[0].value,
                                            reach28: result3.data[1].values[0].value,
                                            profileviews: result5.data[0].values[0].value,
                                            emailcontacts: result5.data[2].values[0].value,
                                            followerscountday: result5.data[1].values[0].value,
                                            ages: resultInfo.ages,
                                            agegroup: maxIndex,
                                            female: resultInfo.female,
                                            male: resultInfo.male,
                                            maleRatio: resultInfo.male/(resultInfo.male + resultInfo.female)
                                        }},
                                     (error, result) => {
                                        if (error) {
                                            console.log("Error updating ",user_name,"'s data");
                                            console.log(result);
                                            console.log(error);
                                        }
                                        else {
                                            console.log(result1.username,"'s data updated succesfully");
                                        }
                                        onComplete(error,result);
                                    });
                                }
                            });
                        }

                    }

                }

                catch(errr) {
                    console.log("\n------- Catched unknown error updating", user_name,"'s data ------\n");
                    console.log("error was",errr);
                    console.log("batch callback err ",err);
                    console.log("batch callback res",res);
                    "\n------------------------------------------------------\n"
                    const result = null;
                    onComplete(errr,res);
                }
            });
            Future.wait(future);

            return true;
            }
        else{
            return false;
        }
    },
    UpdateUserDataComplete:  function(insta_id){

        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var doc = Graphusers.findOne({insta_id: insta_id});
        var user_name = doc.usname;

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            var future = new Future();
            var onComplete = future.resolver();
        
            var req1 = insta_id + '?fields=username,followers_count,follows_count,media_count,profile_picture_url,biography';
            var req2 = insta_id + '/insights?metric=impressions,reach&period=week';
            var req3 = insta_id + '/insights?metric=impressions,reach&period=days_28';
            var req4 = insta_id + '/insights?metric=audience_gender_age&period=lifetime';
            var req5 = insta_id + '/insights?metric=profile_views,follower_count,email_contacts&period=day';
            var req6 = insta_id + '/insights?metric=audience_locale,audience_country,audience_city&period=lifetime';

            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                },
                {
                    method: "GET",
                    relative_url: req2
                },
                {
                    method: "GET",
                    relative_url: req3
                },
                {
                    method: "GET",
                    relative_url: req4 
                },
                {
                    method: "GET",
                    relative_url: req5
                },
                {
                    method: "GET",
                    relative_url: req6 
                }
            ],function(err, res) {

                try {
                    if(res.error){
                        console.log("Error updating ",user_name,"'s data");
                        console.log(res.error.message);
                        onComplete(err,res);
                    }
                    else{

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);
                        var result3 = JSON.parse(res[2].body);
                        var result4 = JSON.parse(res[3].body);
                        var result5 = JSON.parse(res[4].body);
                        var result6 = JSON.parse(res[5].body);

                        if ( result1.error || result2.error || result3.error || result4.error || result5.error || result6.error){
                            console.log("Error updating ",user_name,"'s data");
                            console.log("Result 1 error was ", result1.error.message);
                            onComplete(err,res);
                        }
                        else{

                            var genderAndAges = rewriteProperties(result4.data[0].values[0].value);
                            var audiencecity = removeDotsCities(result6.data[2].values[0].value);
                            var resultInfo = processInfo(result4.data[0].values[0].value);
                            var maxIndex = findMaxIndex(resultInfo.ages);
                            const link = "https://www.instagram.com/"+result1.username+"/";

                           
                            Graphusers.rawCollection().findOne({insta_id: insta_id}, function(err,res){

                                if (res){
                                    Graphusers.rawCollection().update({insta_id : insta_id},{$push: {userhistory: {time: Date.now(), 
                                        followers: result1.followers_count,
                                        numposts: result1.media_count,
                                        gender_ages: genderAndAges,
                                        impressions7: result2.data[0].values[0].value,
                                        reach7: result2.data[1].values[0].value,
                                        impressions28: result3.data[0].values[0].value,
                                        reach28: result3.data[1].values[0].value,
                                        profileviews: result5.data[0].values[0].value

                                    } }, $set: {
                                            usname: result1.username,
                                            link: link,
                                            followers: result1.followers_count,
                                            followers_string: result1.followers_count.toLocaleString(),
                                            follows: result1.follows_count,
                                            numposts: result1.media_count,
                                            picurl: result1.profile_picture_url,
                                            locale: result6.data[0].values[0].value,
                                            audiencecountry: result6.data[1].values[0].value,
                                            audiencecity: audiencecity,
                                            gender_ages: genderAndAges,
                                            impressions7: result2.data[0].values[0].value,
                                            reach7: result2.data[1].values[0].value,
                                            impressions28: result3.data[0].values[0].value,
                                            reach28: result3.data[1].values[0].value,
                                            profileviews: result5.data[0].values[0].value,
                                            emailcontacts: result5.data[2].values[0].value,
                                            followerscountday: result5.data[1].values[0].value,
                                            ages: resultInfo.ages,
                                            agegroup: maxIndex,
                                            female: resultInfo.female,
                                            male: resultInfo.male,
                                            maleRatio: resultInfo.male/(resultInfo.male + resultInfo.female)
                                        }}, 
                                     (error, result) => {
                                        if (error) {
                                            console.log("Error updating ",user_name,"'s data");
                                            console.log(result);
                                            console.log(error);
                                        }
                                        else {
                                            console.log(result1.username,"'s data updated succesfully");
                                        }
                                        onComplete(error,result);
                                    });
                                }
                            });
                        }

                    }

                }

                catch(errr) {
                    console.log("\n------- Catched unknown error updating", user_name,"'s data ------\n");
                    console.log("error was",errr);
                    console.log("batch callback err ",err);
                    console.log("batch callback res",res);
                    "\n------------------------------------------------------\n"
                    const result = null;
                    onComplete(errr,res);
                }
            });
            Future.wait(future);

            return true;
            }
        else{
            return false;
        }
    },

    UpdateStoryInfo: function(campaign, id , insta_id){

        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        
        var futurePull = new Future();
        var onCompletePull = futurePull.resolver();

        var arrayObj;
        var doc = Graphusers.findOne({insta_id: insta_id});
        var campaignDoc = Campaigns.findOne({nombre: campaign});

        var posteo = findObjectByKey(campaignDoc.posts, 'id', id);

        var updateCounter = posteo.counter;
        if (posteo.counter == undefined) updateCounter = 0;
        else updateCounter += 1;
        
        console.log("Updating story: ", id ," update step:, ", updateCounter);

        var req1 = id + '/insights?metric=impressions,reach,exits,replies,taps_forward,taps_back';
        var req2 = id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count';

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

                graph.batch([
                {
                    method: "GET",
                    relative_url: req1
                },
                {
                    method: "GET",
                    relative_url: req2
                }
                ], function(err, res){

                    if(err){
                        console.log(err);
                        SyncedCron.remove(id);
                    }
                    else{

                        //console.log("Code responses from batch request (insta story update) , ", res[0].code , " and ", res[1].code);
                        

                        if( res[0].code == 400  || res[1].code == 400 || updateCounter > 23){
                            console.log("Removing chron job");
                            SyncedCron.remove(id);
                        }
                        else{

                            var result1 = JSON.parse(res[0].body);
                            var result2 = JSON.parse(res[1].body);


                            arrayObj = {
                                impressions: result1.data[0].values[0].value,
                                reach: result1.data[1].values[0].value,
                                exits: result1.data[2].values[0].value,
                                replies: result1.data[3].values[0].value,
                                taps_forward: result1.data[4].values[0].value,
                                taps_back: result1.data[5].values[0].value,
                                imgurl: result2.thumbnail_url,
                                media_type: 'S',
                                id: id,
                                usname: doc.usname,
                                followers: doc.followers,
                                counter: updateCounter
                            }


                            Campaigns.rawCollection().update({nombre : campaign} , {$pull: {posts: {id: id } } },
                                (error, result) => {
                                    if (error){
                                        console.log(error);
                                        onCompletePull(error, result);
                                    }
                                    else {
                                        console.log("removed story to update");
                                        onCompletePull(error, result);
                                    }
                            })

                        }

                    }

                });
            }
    Future.wait(futurePull);

    Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },
                            (error, result) => {
                                if (error) {
                                    console.log("Error updating story info");
                                    console.log(err);
                                }
                                else {
                                    console.log("Story updated in campaign posts");
                                }
                            });

    }
});