import { Meteor } from 'meteor/meteor'
import fbgraph from 'fbgraph';
import future from 'fibers/future';



Meteor.methods({
    MediaIds:  function(insta_id, range) {
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var doc = Graphusers.findOne({insta_id: insta_id});
        

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            var future = new Future();
            var onComplete = future.resolver();
            var result1;

            var req1 = insta_id + '/media';

            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                }
            ],function(err, res) {
                console.log("\ngetting media  id's .. \n");
                console.log(err,res);

                result1 = JSON.parse(res[0].body);
                onComplete(err, res);
            });

            Future.wait(future);

            return result1.data.slice(range[0] , range[1]);
        }
        else{
            return false;
        }
    },
    StoriesIds:  function(insta_id) {
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var doc = Graphusers.findOne({insta_id: insta_id});

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            

            var future = new Future();
            var onComplete = future.resolver();

            var longAccesTokenStories = "noTokenAssigned";

            
            var req1 = insta_id + '/stories';
            var result1;


            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                }
            ],function(err, res) {
                console.log("\ngetting stories id's .. \n");

                // Discovering results
                result1 = JSON.parse(res[0].body);
                
                
                onComplete(err, res);
            });

            Future.wait(future);

            return result1;
            }
        else{
            return false;
        }
    },
    
    MediaInfo:  function(ID, insta_id){
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();

        var doc = Graphusers.findOne({insta_id: insta_id});
        var container;

        var req3 = ID + '?fields=id,media_type,thumbnail_url,media_url';

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);


            graph.batch([
                {
                    method: "GET",
                    relative_url: req3// Get insights, impressions, reach WEEK timeframe
                }
            ],function(err, res) {

                if (err){
                    console.log("Error requesting info of media object");
                    console.log(err);
                }
                else{
                    console.log("Getting media info of : ", ID);

                    var result1 = JSON.parse(res[0].body);

                    container = {result1:result1 , id: ID};

                }
                

                onComplete(err, res);
            });
        }

        Future.wait(future);
        return container;
    },

    MediaInfoComplete:  function(obj){
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();

        var doc = Graphusers.findOne({insta_id: obj.insta_id});
        var container;

        var req1 = obj.id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count';
        var req2 = obj.id + '/insights?metric=impressions,reach,engagement,saved,video_views';
        var req3 = obj.id + '/insights?metric=impressions,reach,engagement,saved';

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            if (obj.media_type == 'VIDEO'){

                graph.batch([
                    {
                        method: "GET",
                        relative_url: req1
                    },
                    {
                        method: "GET",
                        relative_url: req2
                    }
                ],function(err, res) {

                    if (err){
                        console.log("Error requesting info of media object");
                        console.log(err);
                    }
                    else{
                        //console.log("Getting all info about video: ", obj.id);

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        container = {result1:result1 , result2:result2 , id: obj.id};

                    }
                    

                    onComplete(err, res);
                });
            }
            else{
                graph.batch([
                    {
                        method: "GET",
                        relative_url: req1
                    },
                    {
                        method: "GET",
                        relative_url: req3
                    }
                ],function(err, res) {

                    if (err){
                        console.log("Error requesting info of media object");
                        console.log(err);
                    }
                    else{
                        //console.log("Getting all info about photo: ", obj.id);

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        container = {result1:result1 , result2:result2 , id: obj.id};

                    }
                    

                    onComplete(err, res);
                });
            }
        }

        Future.wait(future);
        return container;
    },

    MediaInfoCompleteComments:  function(obj){
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();

        var doc = Graphusers.findOne({insta_id: obj.insta_id});
        var container;

        var req1 = obj.id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count,caption,comments,permalink,shortcode';
        var req2 = obj.id + '/insights?metric=impressions,reach,engagement,saved,video_views';
        var req3 = obj.id + '/insights?metric=impressions,reach,engagement,saved';

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            if (obj.media_type == 'VIDEO'){

                graph.batch([
                    {
                        method: "GET",
                        relative_url: req1
                    },
                    {
                        method: "GET",
                        relative_url: req2
                    }
                ],function(err, res) {

                    if (err){
                        console.log("Error requesting info of media object");
                        console.log(err);
                    }
                    else{
                        //console.log("Getting all info about video: ", obj.id);

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        container = {result1:result1 , result2:result2 , id: obj.id};

                    }
                    

                    onComplete(err, res);
                });
            }
            else{
                graph.batch([
                    {
                        method: "GET",
                        relative_url: req1
                    },
                    {
                        method: "GET",
                        relative_url: req3
                    }
                ],function(err, res) {

                    if (err){
                        console.log("Error requesting info of media object");
                        console.log(err);
                    }
                    else{
                        //console.log("Getting all info about photo: ", obj.id);

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        container = {result1:result1 , result2:result2 , id: obj.id};

                    }
                    

                    onComplete(err, res);
                });
            }
        }

        Future.wait(future);
        return container;
    },

    StoryInfo:  function(story_id, insta_id){
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();

        var doc = Graphusers.findOne({insta_id: insta_id});
        var container;


        var req1 = story_id + '/insights?metric=impressions,reach,exits,replies,taps_forward,taps_back';
        var req2 = story_id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count';

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);


            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                },
                {
                    method: "GET",
                    relative_url: req2// Get insights, impressions, reach WEEK timeframe
                }
            ],function(err, res) {

                if (err){
                    console.log("Error requesting info of story");
                    console.log(err);
                }
                else{
                    console.log("Getting story info of: ", story_id);


                    // Discovering results
                    var result1 = JSON.parse(res[0].body);
                    var result2 = JSON.parse(res[1].body);

                    var retencion = 100 * (1-result1.data[2].values[0].value / result1.data[0].values[0].value);
                    var ret = retencion.toFixed(1) + " %";

                    var timems = (Date.now() - Date.parse(result2.timestamp))/1000/60/60;
                    var since = timems.toFixed(1) + " h";

                    container = {result1:result1 , result2:result2 , id: story_id , ret: ret, since: since};

                }
                

                onComplete(err, res);
            });
        }

        Future.wait(future);
        return container;
    }
});


