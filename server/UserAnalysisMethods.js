import { Meteor } from 'meteor/meteor'
import fbgraph from 'fbgraph';
import future from 'fibers/future';



UserAnalysis = new Mongo.Collection('useranalysis');

Meteor.methods({

    ChainedMediaCallTrendsetters: function(insta_id) {
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        Graphusers.find({trendsetter: 1}).forEach( function(doc) {

            var insta_id = doc.insta_id;

            Meteor.call('ChainedMediaCall', insta_id ,function(err,res) {
                
                if (err){
                    console.log(err);
                }
            })

        })
    },

    ChainedMediaCall: function(insta_id) {

        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var futureGetIds = new Future();
        var onCompleteGetIds = futureGetIds.resolver();

        var ids;

        var doc = Graphusers.findOne({insta_id: insta_id});
        var usname = doc.usname;


        console.log("\n---------------- STARTING MEDIA REQUEST ----------------");
        Meteor.call('GetAllMediaIds', insta_id, function(err, res){
            ids = res.data;
            //console.log(res);
            onCompleteGetIds(err,res);
        });
        Future.wait(futureGetIds);
        //console.log(ids);

        var UserDoc = UserAnalysis.findOne({usname: usname});

        if (!UserDoc){

            var futureArrayCreation = new Future();
            var onCompleteArrayCreation = futureArrayCreation.resolver();

            UserAnalysis.rawCollection().insert({usname : usname , medias: []},
                        (error, result) => {
                            if (error) {
                                console.log("Error creating array")
                                onCompleteArrayCreation(error,result);
                            }
                            else {
                                console.log("Creating medias array");
                                onCompleteArrayCreation(error,result);
                            }
                        });
            Future.wait(futureArrayCreation);
        }

        



        if (ids){
            for(i = 0; i < ids.length; i++){

                

                var userDoc = UserAnalysis.findOne({usname: usname});
                var isAlreadySaved = findObjectByKey(userDoc.medias , 'id' , ids[i].id);

                if (isAlreadySaved){
                    console.log("post data already saved");
                }
                else{

                    try{


                        //------------------------ MEDIA TYPE --------------------------------
                        
                        var futureMediaType = new Future();
                        var onCompleteMediaType = futureMediaType.resolver();

                        var isvideo = false;
                        var media_type;

                        Meteor.call('MediaInfo',ids[i].id , insta_id , function(er, resu){
                            //console.log("Checking media type for, ", ids[i]);
                            media_type = resu.result1.media_type;
                            if (resu.result1.media_type = 'VIDEO'){
                                isvideo = true
                                //console.log("its a video")
                            }
                            //else console.log("its a photo or carousel");
                            onCompleteMediaType(er,resu);
                        });

                        Future.wait(futureMediaType);

                        //------------------------ INFO COMPLETE --------------------------------

                        var futureInfoComplete = new Future();
                        var onCompleteInfoComplete = futureInfoComplete.resolver();

                        var obj = { id: ids[i].id , insta_id: insta_id , media_type: media_type }
                        Meteor.call('MediaInfoCompleteComments',obj, function(e, r){
                            
                            var post = {
                                impressions: r.result2.data[0].values[0].value,
                                reach: r.result2.data[1].values[0].value,
                                engagement: r.result2.data[2].values[0].value,
                                saved: r.result2.data[3].values[0].value,
                                like_count: r.result1.like_count,
                                comments_count: r.result1.comments_count,
                                media_type: obj.media_type,
                                id: obj.id,
                                usname: usname,
                                media_url: r.result1.thumbnail_url,
                                followers: doc.followers,
                                timestamp: r.result1.timestamp,
                                permalink: r.result1.permalink,
                                shortcode: r.result1.shortcode,
                                comments: r.result1.comments,
                                caption: r.result1.caption
                            };
                            //console.log(post);

                            if (obj.media_type == 'VIDEO') post.video_views = r.result2.data[4].values[0].value;

                            
                            UserAnalysis.rawCollection().update({usname : usname } , {$push: {medias: post } },
                            (error, result) => {
                                if (error) {
                                    console.log("Error inserting media to array")
                                }
                                else {
                                    console.log("Media pushed in array of , ",usname);
                                }
                            });
                            onCompleteInfoComplete(e,r);
                        });

                        Future.wait(futureInfoComplete);
                    }
                    catch(err){
                        console.log("Error getting post info");
                    }
                }
                
            }
        }
        else{
            console.log("No media ids where found");
        }
    },

	GetAllMediaIds: function(insta_id){
		var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var doc = Graphusers.findOne({insta_id: insta_id});
        

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            var future = new Future();
            var onComplete = future.resolver();
            var result1;

            var req1 = insta_id + '/media';

            graph.batch([
                {
                    method: "GET",
                    relative_url: req1// Get the basic insta info
                }
            ],function(err, res) {
                console.log("\ngetting all media  id's .. \n");

                result1 = JSON.parse(res[0].body);
                //console.log(result1);
                onComplete(err, res);
            });

            Future.wait(future);

            return result1;
        }
        else{
            return false;
        }
    
	},

    MediaInfoCompleteInsert:  function(obj){
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();

        var doc = Graphusers.findOne({insta_id: obj.insta_id});
        var container;

        var req1 = obj.id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count';
        var req2 = obj.id + '/insights?metric=impressions,reach,engagement,saved,video_views';
        var req3 = obj.id + '/insights?metric=impressions,reach,engagement,saved';

        if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            if (obj.media_type == 'VIDEO'){

                graph.batch([
                    {
                        method: "GET",
                        relative_url: req1
                    },
                    {
                        method: "GET",
                        relative_url: req2
                    }
                ],function(err, res) {

                    if (err){
                        console.log("Error requesting info of media object");
                        console.log(err);
                    }
                    else{
                        console.log("Getting all info about video: ", obj.id);

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        container = {result1:result1 , result2:result2 , id: obj.id};

                    }
                    

                    onComplete(err, res);
                });
            }
            else{
                graph.batch([
                    {
                        method: "GET",
                        relative_url: req1
                    },
                    {
                        method: "GET",
                        relative_url: req3
                    }
                ],function(err, res) {

                    if (err){
                        console.log("Error requesting info of media object");
                        console.log(err);
                    }
                    else{
                        console.log("Getting all info about photo: ", obj.id);

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        container = {result1:result1 , result2:result2 , id: obj.id};

                    }
                    

                    onComplete(err, res);
                });
            }
        }

        Future.wait(future);
        return container;
    },
})