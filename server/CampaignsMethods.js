import { Meteor } from 'meteor/meteor'
import future from 'fibers/future';
import later from 'later';

    
function findAndRemove(array, property, value) {
  array.forEach(function(result, index) {
    if(result[property] === value) {
      //Remove from array
      array.splice(index, 1);
    }    
  });
}

function addSynchStory(id , campaign, insta_id) {
    SyncedCron.add({

      name: id,

      schedule: function() {
        var later = Npm.require('later');
        return later.parse.recur().every(1).hour();
      },
      job: function() {


        Meteor.call('UpdateStoryInfo', campaign , id, insta_id , function(err,res) {
            
            if (err){
                console.log(err);
            }
        })
      }
    });
}

Meteor.methods({
    CreateCampaign:  function(obj) {

        console.log("Creating campaign");
        
        Campaigns.insert({
            nombre: obj.nombre,
            cliente: obj.cliente,
            fecha: obj.fecha,
            central: obj.central,
            posts: [],
            propuestas: [],
            briefs: []
        });
    },
    addImage:  function(obj) {

        console.log(obj.data, obj.name , obj.campaignname);


        Campaigns.rawCollection().update({nombre : obj.campaignname} , {$push: {posts: {name: obj.name , data: obj.data} } },
                        (error, result) => {
                            if (error){
                                console.log(error)
                            }
                        })
        
    },
    sendEmail: function(subject, text) {

        var postmark = require("postmark");

        var to = "mpozzi@trendsetters.com";
        var from = "mpozzi@trendsetters.com";

        console.log("Sending email to: ", to);

        var client = new postmark.Client("86b14040-4483-4b9b-85ce-48de30fbde40");

        client.sendEmail({
          "From": from,
          "To": to,
          "Subject": subject,
          "TextBody": text
        });
    },
    AddBrief: function(campaignname, nombrebrief, brief) {

        Campaigns.rawCollection().update({nombre : campaignname} , {$push: {briefs: {nombrebrief: nombrebrief , brief: brief} } },
                        (error, result) => {
                            if (error){
                                console.log(error)
                            }
                        })

    },
    EditPostData: function(campaignname, obj) {
        
        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();


        var doc = Campaigns.findOne({nombre: campaignname});

        Campaigns.rawCollection().update({nombre : campaignname} , {$pull: {posts: {id: obj.id } } },
            (error, result) => {
                if (error){
                    console.log(error);
                    onComplete(error, result);
                }
                else {
                    console.log("removed post temporarily");
                    onComplete(error, result);
                }
        })

        Future.wait(future);


        if (obj.contenttype == "FOTO") obj.media_type = 'I';
        if (obj.contenttype == "VIDEO") obj.media_type = 'V';

        if (obj.contenttype == "STORY") {
            obj.media_type = 'S';
            var arrayPost = {
                impressions: obj.nroimpr,
                reach: obj.alcanceu,
                media_type: obj.media_type,
                id: obj.id,
                usname: obj.usname,
                followers: obj.nroseguidores,
                exits: obj.nroexits,
                taps_forward: obj.tapf,
                taps_back: obj.tapb,
                replies: obj.nroreplies,
                nroclicks: obj.nroclicks
            }
        }

        if (obj.contenttype == "FOTO" || obj.contenttype == "VIDEO"){
            var arrayPost = {
                impressions: obj.nroimpresiones,
                reach: obj.alcanceu,
                engagement: obj.engagement,
                media_type: obj.media_type,
                id: obj.id,
                usname: obj.usname,
                followers: obj.nroseguidores,
                like_count: obj.nrolikes,
                comments_count: obj.nrocomentarios,
                saved: obj.nroguardados
            }

            if(obj.contenttype == "FOTO") arrayPost.video_views = null;
            if(obj.contenttype == "VIDEO") arrayPost.video_views = obj.nroviews;
        }
        

        Campaigns.rawCollection().update({nombre : campaignname} , {$push: {posts: arrayPost } },
            (error, result) => {
                if (error){
                    console.log(error);
                }
                else {
                    console.log("added edited post");
                }
        })

    },
	AddPost: function(campaign, obj){

		var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();

        console.log("Adding post , ",obj.id);

		var doc = Graphusers.findOne({insta_id: obj.insta_id});

		var req1 = obj.id + '/insights?metric=impressions,reach,engagement,saved';
		var req2 = obj.id + '/insights?metric=impressions,reach,engagement,saved,video_views';
        var req3 = obj.id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count';

		if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            if (obj.media_type == 'VIDEO'){
            	graph.batch([
                {
                    method: "GET",
                    relative_url: req2
                },
                {
                    method: "GET",
                    relative_url: req3
                }
                ], function(err, res){

                	var result1 = JSON.parse(res[0].body);
                	var result2 = JSON.parse(res[1].body);


                	arrayObj = {
                		impressions: result1.data[0].values[0].value,
                        reach: result1.data[1].values[0].value,
                        engagement: result1.data[2].values[0].value,
                        saved: result1.data[3].values[0].value,
                        video_views: result1.data[4].values[0].value,
                        like_count: result2.like_count,
                        comments_count: result2.comments_count,
                        media_type: 'V',
                        id: obj.id,
                        usname: doc.usname,
                        media_url: result2.thumbnail_url,
                        followers: doc.followers
                	}

                	Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },
                		(error, result) => {
                            if (error) {
                                console.log("Error inserting post to campaign")
                            }
                            else {
                                console.log("Post inserted in campaign");
                            }
                        });

                	onComplete(err,res);

                });
            }
            else{
            	graph.batch([
                {
                    method: "GET",
                    relative_url: req1
                },
                {
                    method: "GET",
                    relative_url: req3
                }
                ], function(err, res){

                	var result1 = JSON.parse(res[0].body);
                	var result2 = JSON.parse(res[1].body);


                	arrayObj = {
                		impressions: result1.data[0].values[0].value,
                        reach: result1.data[1].values[0].value,
                        engagement: result1.data[2].values[0].value,
                        saved: result1.data[3].values[0].value,
                        video_views: null,
                        like_count: result2.like_count,
                        comments_count: result2.comments_count,
                        media_type: 'I',
                        id: obj.id,
                        usname: doc.usname,
                        media_url: result2.media_url,
                        followers: doc.followers
                	}


                	Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },
                		(error, result) => {
                            if (error) {
                                console.log("Error inserting post to campaign")
                            }
                            else {
                                console.log("Post inserted in campaign");
                            }
                        });

                	onComplete(err,res);

                });
            }

		}
		Future.wait(future);
	},

	AddStory: function(campaign, obj){

		var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var Fiber = require('fibers');

        var future = new Future();
        var onComplete = future.resolver();
        var future1 = new Future();
        var onComplete1 = future1.resolver();

        console.log("Adding story ..")

		var doc = Graphusers.findOne({insta_id: obj.insta_id});

        var req1 = obj.id + '/insights?metric=impressions,reach,exits,replies,taps_forward,taps_back';
        var req2 = obj.id + '?fields=id,media_type,thumbnail_url,media_url,owner,timestamp,comments_count,like_count';

		if(doc.accesstoken) {
            graph.setVersion("2.10");
            graph.setAccessToken(doc.accesstoken);

            	graph.batch([
                {
                    method: "GET",
                    relative_url: req1
                },
                {
                    method: "GET",
                    relative_url: req2
                }
                ], function(err, res){

                    if(err){
                        console.log(err);
                        onComplete(err,res);
                    }
                    else{

                        var result1 = JSON.parse(res[0].body);
                        var result2 = JSON.parse(res[1].body);

                        if (result2.media_type == 'VIDEO') var url_s = result2.thumbnail_url;
                        if (result2.media_type == 'IMAGE') var url_s = result2.media_url;

                        console.log(url_s);


                        arrayObj = {
                            impressions: result1.data[0].values[0].value,
                            reach: result1.data[1].values[0].value,
                            exits: result1.data[2].values[0].value,
                            replies: result1.data[3].values[0].value,
                            taps_forward: result1.data[4].values[0].value,
                            taps_back: result1.data[5].values[0].value,
                            media_url: url_s,
                            media_type: 'S',
                            id: obj.id,
                            usname: doc.usname,
                            followers: doc.followers
                        }

                        Fiber(function () {
                            addSynchStory(obj.id , campaign, obj.insta_id);
                        }).run();
                        
                        
                        Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },
                            (error, result) => {
                                if (error) {
                                    console.log("Error inserting story to campaign")
                                    onComplete1(err,res);
                                }
                                else {
                                    console.log("Story inserted in campaign");

                                    

                                    onComplete1(err,res);

                                }
                            });

                        

                        onComplete(err,res);
                        }

                	

                });
            }
        Future.wait(future1)
		Future.wait(future);
	},


    AddManual: function(campaign , obj){

        //console.log(obj);

        if (obj.contenttype == '0'){ // VIDEO

            var arrayObj = {
                impressions: obj.nroimpresiones,
                reach: obj.alcanceu,
                engagement: null,
                saved: obj.nroguardados,
                video_views: obj.nroviews,
                like_count: obj.nrolikes,
                comments_count: obj.nrocomentarios,
                media_type: 'V',
                usname: obj.usname,
                followers: obj.nroseguidores,
                id: obj.id
            }

            Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },

                            (error, result) => {
                                if (error) {
                                    console.log("Error inserting manual video to campaign")
                                }
                                else {
                                    console.log("Manual video inserted in campaign");
                                }
                            });
        }

        if (obj.contenttype == '1'){ // FOTO

            const engagement = obj.nrolikes + obj.nroguardados + obj.nrocomentarios;

            var arrayObj = {
                impressions: obj.nroimpresiones,
                reach: obj.alcanceu,
                engagement: engagement,
                saved: obj.nroguardados,
                video_views: null,
                like_count: obj.nrolikes,
                comments_count: obj.nrocomentarios,
                media_type: 'I',
                usname: obj.usname,
                followers: obj.nroseguidores,
                id: obj.id
            }
            
            Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },

                            (error, result) => {
                                if (error) {
                                    console.log("Error inserting manual image to campaign")
                                }
                                else {
                                    console.log("Manual image inserted in campaign");
                                }
                            });
        }

        if (obj.contenttype == '2'){ // INSTA STORY

            var arrayObj = {
                impressions: obj.nroimpr,
                reach: obj.alcanceu,
                engagement: null,
                saved: null,
                video_views: null,
                like_count: null,
                comments_count: null,
                media_type: 'S',
                usname: obj.usname,
                followers: obj.nroseguidores,
                nroclicks: obj.nroclicks,
                porret: obj.porret,
                id: obj.id
            }
            
            Campaigns.rawCollection().update({nombre : campaign} , {$push: {posts: arrayObj } },

                            (error, result) => {
                                if (error) {
                                    console.log("Error inserting manual story to campaign")
                                }
                                else {
                                    console.log("Manual story inserted in campaign");
                                }
                            });
        }

    },
    RemoveMedia: function(campaign , id){

        var doc = Campaigns.findOne({nombre : campaign});

        console.log('Removing ', id , ' post/story from ', campaign);

        var newArray = doc.posts;

        findAndRemove(newArray, 'id', id)

        Campaigns.rawCollection().update({nombre : campaign} , { $set: { posts: newArray } } ,

            function(err , res){

                if (err) console.log(err)

            });
    },
    AddPropuesta: function(campaignname, obj){

        var doc = Graphusers.findOne({usname: obj.usname});

        obj.followers = doc.followers;
        obj.reach28 = doc.reach28;
        obj.impressions28 = doc.impressions28;

        Campaigns.rawCollection().update({nombre : campaignname} , {$push: {propuestas: obj } },
                
                (error, result) => {
                    if (error){
                        console.log(error)
                    }
                    else console.log("Added propuesta")
                })
    },
    AddPropuestaManual: function(campaignname, obj){

        Campaigns.rawCollection().update({nombre : campaignname} , {$push: {propuestas: obj } },
                
                (error, result) => {
                    if (error){
                        console.log(error)
                    }
                    else console.log("Added propuesta manual")
                })
    },
    UpdatePropData: function(campaignname , obj){

        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();


        Campaigns.rawCollection().update({nombre : campaignname} , {$pull: {propuestas: {usname: obj.usname } } },
            (error, result) => {
                if (error){
                    console.log(error);
                    onComplete(error, result);
                }
                else {
                    console.log("removed prop temporarily");
                    onComplete(error, result);
                }
        })

        Future.wait(future);

        Campaigns.rawCollection().update({nombre : campaignname} , {$push: {propuestas: obj } },
            (error, result) => {
                if (error){
                    console.log(error);
                }
                else {
                    console.log("added edited prop");
                }
        })
    },
    RemovePropuesta: function(campaign , usname){

        var doc = Campaigns.findOne({nombre : campaign});

        var newArray = doc.propuestas;

        findAndRemove(newArray, 'usname', usname);
        
        Campaigns.rawCollection().update({nombre : campaign} , { $set: { propuestas: newArray } } ,

            function(err , res){

                if (err) console.log(err);
                else console.log("Removed propuesta");

            });
    },
    ExportPDF: function(campaignname) {
        console.log("Exporting to pdf ");

        var doc = Campaigns.findOne({nombre: campaignname});

        var data = [1,2,3]
        var fields = [
            {
              key: 'usname',
              title: 'Username'
            },
            {
              key: 'followers',
              title: 'Followers',
              type: 'number'
            }
          ];

        var title = 'PostsTable';
        var file = Excel.export(title, fields, doc);
        var headers = {
            'Content-type': 'application/vnd.openxmlformats',
            'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
        };
    }
});


