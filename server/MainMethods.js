import { Meteor } from 'meteor/meteor'
import { HTTP } from 'meteor/http'

import fbgraph from 'fbgraph';
import future from 'fibers/future';



// ------------------------ Collection ---------- ---------------------
Thinkyusers = new Mongo.Collection('thinkyusers');



Meteor.methods({
    
    ValidateLogin:  function(Username, Passwrd){

        console.log("validating login: ", Username ," , " , Passwrd);
        //console.log(Username);
        //console.log(Passwrd);

        var bcrypt = require('bcrypt');
        const saltRounds = 10;
        var res = false;

        var Future = Npm.require('fibers/future');
        var future = new Future();
        var onComplete = future.resolver();
        
        if (Thinkyusers.findOne({usname:Username})){

            
            const doc = Thinkyusers.findOne({usname:Username});
            const hash = doc.hash;
            //console.log("Found username");

            bcrypt.compare(Passwrd, hash, function(er, result) {
            
            if (result){
                //console.log("Password matched");
                res = result;
                onComplete(er,result);
                
            }
            else{
                //console.log("Password not found in database");
                onComplete(er,result);
            }   
        });
            
        }
        else{
            console.log("Username not in our database");
            const er = "Username not in our database";
            const result = false;
            onComplete(er,result);
        }
        Future.wait(future);
        //console.log("END METHOD");
        return res;
    },

    ChainedCall: function() {

        var graph = Npm.require('fbgraph');
        var Future = Npm.require('fibers/future');

        var future = new Future();
        var onComplete = future.resolver();
        var ids_names;

        console.log("\n---------------- STARTING REQUEST ----------------\nGetting pages of profile\n");
        Meteor.call('GetAccounts', function(err, res){
            ids_names = res;
            onComplete(err,res);
        });
        Future.wait(future);

        if (ids_names){
            for(i = 0; i < ids_names.ids.length; i++){
                var aux_future = new Future();
                var aux_onComplete = aux_future.resolver();
                var aux_future1 = new Future();
                var aux_onComplete1 = aux_future1.resolver();
                var makerequest = false;

                Meteor.call('IsInstaConnected',ids_names.ids[i] , function(er, resu){
                console.log("Checking if theres instagram account associated to , ", ids_names.names[i]);
                makerequest = resu;
                if (makerequest != false) console.log("There is account connected to ", ids_names.names[i]);
                else console.log("No instagram bussiness associated to ", ids_names.names[i]);
                aux_onComplete(er,resu);
                });

                Future.wait(aux_future);

                if (makerequest != false){
                    Meteor.call('SubInfoRequest',makerequest, function(e, r){
                        aux_onComplete1(e,r);
                    });
                    Future.wait(aux_future1);
                }
            }
        }
        else{
            if ( Meteor.user().emails[0].address){
                console.log("No accounts for ", Meteor.user().emails[0].address);    
            }
            else{
                console.log("Either no accounts or bad login");
            }
            
        }
    }
});


