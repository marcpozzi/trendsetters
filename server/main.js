import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  rewriteProperties = function(obj) {
	    if (typeof obj !== "object") return obj;
	    for (var prop in obj) {
	        if (obj.hasOwnProperty(prop)) {
	            obj[prop.replace(/\./g, ":")] = rewriteProperties(obj[prop]);
	            if (prop.indexOf(".") > -1) {
	                delete obj[prop];
	            }
	        }
	    }
	    return obj;
	}
	
	processInfo = function (obj) {
	    var resultInfo = {};
	    resultInfo.female = 0;
	    resultInfo.male = 0;
	    resultInfo.ages = [0,0,0,0,0,0];

	    for (var prop in obj) {
	        
	        if (prop.charAt(0) == 'F') resultInfo['female'] += obj[prop];
	        if (prop.charAt(0) == 'M') resultInfo['male'] += obj[prop];

	        if (prop.charAt(2) == 1 && prop.charAt(5) == 1) resultInfo.ages[0] += obj[prop];
	        if (prop.charAt(2) == 1 && prop.charAt(5) == 2) resultInfo.ages[1] += obj[prop];
	        if (prop.charAt(2) == 2) resultInfo.ages[2] += obj[prop];
	        if (prop.charAt(2) == 3) resultInfo.ages[3] += obj[prop];
	        if (prop.charAt(2) == 4) resultInfo.ages[4] += obj[prop];
	        if (prop.charAt(2) == 5) resultInfo.ages[5] += obj[prop];
	    }
	    return resultInfo;
	}

	findMaxIndex = function (array) {
	  var greatest;
	  var indexOfGreatest;
	  for (var i = 0; i < array.length; i++) {
	    if (!greatest || array[i] > greatest) {
	      greatest = array[i];
	      indexOfGreatest = i;
	    }
	  }
	  return indexOfGreatest;
	}

	findObjectByKey = function (array, key, value) {
	    for (var i = 0; i < array.length; i++) {
	        if (array[i][key] === value) {
	            return array[i];
	        }
	    }
	    return null;
	}

	findAndRemove = function (array, property, value) {
	  array.forEach(function(result, index) {
	    if(result[property] === value) {
	      //Remove from array
	      array.splice(index, 1);
	    }    
	  });
	}

	removeDotsCities = function (cities) {

	    var citiesEdit = {}

	    for (var key in cities) {
		    if (cities.hasOwnProperty(key)) {

		    	var oldKey = key;
		    	key = key.replace(/\./g,'');

		        citiesEdit[key] = cities[oldKey];
		    }
		}
		return citiesEdit;
	}


  	SyncedCron.start();

  	SyncedCron.config({
    	log: false
  	});

});
