


Router.route('/', {
    template: 'home'
});
Router.route('/privacypolicy');
Router.route('/termsofservice');
Router.route('/all');
Router.route('/useranalytics');
Router.route('/campaign');
Router.route('/adminpanel');
Router.route('/email');
Router.route('/grid');
Router.route('/datacontrol');


Router.route('useranalytics/:clickedname', function () {
    this.render('useranalytics', {

        data: function () {
	        return this.params;
        }
    });
});

Router.route('campaign/:campaignhash', function () {
    this.render('campaign', {

        data: function () {
            return this.params;
        }
    });
});



